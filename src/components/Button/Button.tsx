import styles from "./Button.module.scss"
import { FC } from 'react';
import classNames from "classnames";

interface IProps {
    classname?: string,
    text: string,
    onClick?: () => void
}

export const Button: FC<IProps> = ({
    classname,
    text,
    onClick
}) => {
    return(
        <button className={classNames(classname, styles.button)} onClick={onClick}>
            {text}
        </button>
    )
}