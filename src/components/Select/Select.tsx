import styles from "./Select.module.scss"
import { FC, useState } from 'react';
import classNames from "classnames";

interface IOption {
    key: string;
    label: string;
    isFavorite?: boolean;
}

interface IProps {
    options: IOption[];
    value: string;
    onChange: (newValue: string) => void
}

export const Select: FC<IProps> = ({
    options,
    value,
    onChange
}) => {
    const [showDropdown, setShowDropdown] = useState(false)

    const handleClick = () => {
        setShowDropdown(!showDropdown)
    }

    const handleSelect = (option: IOption) => {
        return () => {
            onChange(option.key)
            setShowDropdown(false)
        }
    }

    return (
        <div className={styles.select}>
            <div onClick={handleClick} className={styles.select__main}>
                {options.find((item) => item.key == value)?.label}
                <img src="/images/arrow.svg" alt="" className={classNames(styles.select__mainImg, {
                    [styles.select__mainImg_active]: showDropdown
                })} />
            </div>

            {showDropdown && (
                <div className={styles.select__dropdown}>
                    {options.map((option) => {
                        return (
                            <div
                                key={option.key}
                                className={styles.select__dropdownItem}
                                onClick={handleSelect(option)}
                            >
                                <span>{option.label}</span>
                                <button className={classNames(styles.select__dropdownItemStar, {
                                    [styles.select__dropdownItemStar_active]: option.isFavorite
                                })}/>
                            </div>
                        )
                    })}
                </div>
            )}
        </div>
    )
}