import styles from "./SelectSearch.module.scss"
import { FC, useState } from 'react';
import classNames from "classnames";

interface IOption {
    key: string;
    label: string;
    isFavorite?: boolean;
}

interface IProps {
    options: IOption[];
    value: string;
    onChange: (newValue: string) => void
}

export const SelectSearch: FC<IProps> = ({
    options,
    value,
    onChange
}) => {
    const [showDropdown, setShowDropdown] = useState(false)

    const handleClick = (e: any) => {
        if(e.target.tagName.toLowerCase() != "input") {
            setShowDropdown(!showDropdown)
        }
    }

    const handleSelect = (option: IOption) => {
        return () => {
            onChange(option.key)
            setShowDropdown(false)
        }
    }

    const [search, setSearch] = useState("")

    const handleSearch = (e: any) => {
        setSearch(e.target.value)
    }

    const filteredOptions = options.filter((item)=>{
        return (item.label.toLowerCase().indexOf(search.toLowerCase()) != -1)
    })

    return (
        <div className={styles.select}>
            <div onClick={handleClick} className={styles.select__main}>
                {showDropdown ? (
                    <input
                        placeholder="Введите название"
                        className={styles.select__mainInput}
                        value={search}
                        onChange={handleSearch}
                    />
                ) : (
                    options.find((item) => item.key == value)?.label
                )}
                <img src="/images/arrow.svg" alt="" className={classNames(styles.select__mainImg, {
                    [styles.select__mainImg_active]: showDropdown
                })} />
            </div>

            {showDropdown && (
                <div className={styles.select__dropdown}>
                    {filteredOptions.length > 0 ? (
                        filteredOptions.map((option) => {
                            return (
                                <div
                                    key={option.key}
                                    className={styles.select__dropdownItem}
                                    onClick={handleSelect(option)}
                                >
                                    <span>{option.label}</span>
                                    <button className={classNames(styles.select__dropdownItemStar, {
                                        [styles.select__dropdownItemStar_active]: option.isFavorite
                                    })} />
                                </div>
                            )
                        })
                    ) : (
                        <p className={styles.select__dropdownNull}>
                            Ничего не нашлось :(
                        </p>
                    )}
                </div>
            )}
        </div>
    )
}