import { SelectSearch } from '@/components/SelectSearch/SelectSearch'
import styles from '@/styles/Home.module.scss'
import { useState } from 'react'

export default function Home() {
  const[projectId,setProjectId]=useState("zxzcvbnm")
  return (
    <main className={styles.main}>
        <SelectSearch 
          options={[
            {
              key: "zxzcvbnm",
              label: "New.Project"
            },
            {
              key: "asdfgh",
              label: "Interior Web",
              isFavorite: true
            },
            {
              key: "qwerty",
              label: "CMS project"
            }
          ]}
          value={projectId}
          onChange={setProjectId}
        />
    </main>
  )
}
